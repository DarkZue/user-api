<?php
namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;

class authJWT
{
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parsetoken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json([
                    'success' => 0,
                    'error'   => 'Token is Invalid'
                ]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json([
                    'success' => 0,
                    'error'   => 'Token is Expired'
                ]);
            }else{
                return response()->json([
                    'success' => 0,
                    'error' => 'Unauthorized, Please Insert Authentication Token'
                ]);
            }
        }

        return $next($request);
    }
}