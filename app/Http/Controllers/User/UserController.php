<?php

namespace App\Http\Controllers\User;

use JWTAuth;
use JWTAuthException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use Illuminate\Http\Request;
use App\Auth\User;
use App\Inquiry;
use App\Http\Controllers\API\APIBaseController as APIBaseController;

class UserController extends APIBaseController
{
    public function login(Request $request)
    {
        $request['login_id'] = $request->id;
        $input = $request->only('login_id', 'id', 'password');
        $credentials = $request->only('login_id', 'password');
        $token = null;

        $validator = Validator::make($input, [
            'id' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $customClaim = ["cbj.close"];

        try {
            if (!$token = JWTAuth::attempt($credentials, $customClaim)) {

                return response()->json([
                    'response' => 'error',
                    'message' => 'Invalid Id Or Password',
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ]);
        }

        return $this->sendTokenResponse($token, 'Successful Login');
    }

    public function register(Request $request)
    {
        $input = $request->only('login_id', 'password');
        $validator = Validator::make($input, [
            'login_id' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $user = new User();
        $user->login_id = $request->login_id;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }

    public function getSummary(Request $request)
    {
        $inquiries = new Inquiry();
        $limit  = !empty($request->limit) ? $request->limit : 100;
        $offset = !empty($request->offset) ? $request->offset : NULL;

        if (empty($request->bearerToken())) {
            return $this->sendError('Unauthorized, Please Insert Authentication Token');
        }

        try {
            JWTAuth::authenticate($request->input('token'));

            $data = [];
            $summarises = $inquiries
                ->orderBy('created_at', 'desc')
                ->offset($offset)
                ->limit($limit)
                ->get()->toArray();

            foreach ($summarises as $summary) {
                $date = date('m-d-Y h:i:s', strtotime($summary['created_at']));
                $summary['date'] = $date;
                unset($summary['created_at']);

                $data[] = $summary;
            }

            $total  = count($summarises);

            return $this->sendSummaryResponse($data, $total, (int)$request->limit, (int)$request->offset);
        } catch (JWTException $e) {
            return $this->sendError('Unauthorized');
        }
    }
}
