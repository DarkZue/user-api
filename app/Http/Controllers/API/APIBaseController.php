<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class APIBaseController extends Controller
{
    public function sendTokenResponse($result, $message)
    {
        $response = [
            'success' => 1,
            'token'   => $result,
            'message' => $message,
        ];

        return response()->json($response);
    }

    public function sendSummaryResponse($data, $total, $limit, $offset)
    {
        $response = [
            'success' => 1,
            'message' => 'Successful To Retrieve Data',
            'max'     => $total,
            'data'    => $data,
            'limit'   => $limit,
            'offset'  => $offset
        ];

        return response()->json($response, 200);
    }


    public function sendError($errorMessage)
    {
        $response = [
            'success' => 0,
            'message' => $errorMessage
        ];

        return response()->json($response, 400);

    }
}
