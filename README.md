###### Please Following This Step

1. git clone `https://gitlab.com/DarkZue/user-api.git`

2. Create a new file and file name like this `.env`
    
3. Copy code from `.env.example` and paste on `.env`
    
4. `composer install`
    
5. `php artisan key:generate`
    
6. `php artisan jwt:secret`
